import * as actiontypes from '../actions/actionTypes';
import initialState from '../store/initialState';

const chapterVisibility = (state = initialState.chapterVisibility, action) => {

    switch (action.type) {
        case actiontypes.HIDE_CHAPTERS:
            return false

        case actiontypes.SHOW_CHAPTERS:
            return true
        default:
            return state
    }
}

export default chapterVisibility;
