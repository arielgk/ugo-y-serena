import * as actiontypes from './actionTypes';

export const hideTransition = () => {

    return (dispatch) => {
        dispatch({type: actiontypes.HIDE_TRANSITION});

    }
}

export const showTransition = () => {
    return (dispatch) => {
        dispatch({type: actiontypes.SHOW_TRANSITION});

    }
}
