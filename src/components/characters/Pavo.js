import {Sprite, Text} from '@inlet/react-pixi'
import React from 'react';
import pavo_ske from '../../resource/pavo/pavo_ske';
import pavo_tex from '../../resource/pavo/pavo_tex.json';
import pavo_png from '../../resource/pavo/pavo_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class Pavo extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(pavo_ske);
        factory.parseTextureAtlasData(pavo_tex, PIXI.Texture.fromImage(pavo_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "pavo");

        armatureDisplay.animation.play("animtion0");

        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        armatureDisplay.animation.timeScale = 1;
        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);

        return null
    }
}

export default Pavo;
