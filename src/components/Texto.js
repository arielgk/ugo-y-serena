import React from 'react';
import * as PIXI from "pixi.js";

class Texto extends React.Component {

    render() {

        let text = new PIXI.Text('' + this.props.background, {
            fontFamily: 'Slackey, cursive',
            fontSize: this.props.fontSize ? this.props.fontSize : 60,
            fill: this.props.color ? this.props.color : 0x000,
            align: 'center'
        });

        text.x = this.props.x;
        text.y = this.props.y;

        setTimeout(() => {
            this.props.app.stage.addChild(text);
        }, this.props.timeout);

        return null

    }
}

export default Texto;
