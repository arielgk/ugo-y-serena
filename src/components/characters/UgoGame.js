import {Sprite, Text} from '@inlet/react-pixi'
import React from 'react';
import ugoGame_ske from '../../resource/ugoGame/ugo_game_ske.json';
import ugoGame_tex from '../../resource/ugoGame/ugo_game_tex.json';
import ugoGame_png from '../../resource/ugoGame/ugo_game_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class UgoGame extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(ugoGame_ske);
        factory.parseTextureAtlasData(ugoGame_tex, PIXI.Texture.fromImage(ugoGame_png))

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "ugo_game");

        armatureDisplay.animation.play("timeline");

        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        armatureDisplay.animation.timeScale = 1;
        setTimeout(() => {
            let ble = this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);

        return null
    }
}

export default UgoGame;
