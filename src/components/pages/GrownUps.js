import React from 'react';
import PropTypes from 'prop-types';

import Construction from '../characters/Construction';
import {Container} from '@inlet/react-pixi';
import Background from '../Background';
import Texto from '../Texto';
import grownUpBackground from '../../resource/ui/grownUpBackground2.jpg';
import {connect} from 'react-redux';
import CortinaReverse from "../characters/CortinaReverse";
import grownUpsGrass from "../../resource/ui/grownUpsGrass.png";
import Pavo from '../characters/Pavo';
import ConejoBebe from "../characters/ConejoBebe";
import ConejoMama from "../characters/ConejoMama";
import yuyo from '../../resource/ui/yuyo.png';
import conejo_mama_png from '../../resource/conejo_mama/conejomama_tex.png';
import {isBrowser, isMobile, isTablet} from "react-device-detect";
import pavo_png from '../../resource/pavo/pavo_tex.png';
import cortina_png from '../../resource/burbujas/burbujas-2_tex.png';
import conejo_png from '../../resource/conejo_bebe/conejobebe_tex.png';
import * as PIXI from "pixi.js";
import {bindActionCreators} from "redux";
import * as actions from "../../actions/transitionActions";

class GrownUps extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            characters: [],
        }

        this._resources = [];
        this.mountTimeOut = null;
        this.transitionTimeOut = null;

        this.app = this.props.app;
        this.loadCharacters = this.loadCharacters.bind(this);
        this.loadResources= this.loadResources.bind(this);



        const resources = []
        resources.push(cortina_png,grownUpBackground,conejo_png,yuyo,conejo_mama_png,pavo_png);

        this.loadResources(resources)
        // this.props.actions.showTransition();
    }

    loadCharacters() {

        let textXPos = null;
        let fontSize = null;

        switch(this.props.options.lang){
            case 'es':
                textXPos = 690;
                fontSize = 60;
                break;
            case 'pt':
                textXPos = 720;
                fontSize = 60;
                break;

            default:

                textXPos = 680;
                fontSize = 50;

        }

        this.mountTimeOut = setTimeout(() => {

            let characterPos = {
                Pavo: {x: 1700, y: 870},
            }
            if (isMobile) {
                characterPos = {
                    Pavo: {x: 1750, y: 690},
                }
            }
            if (isTablet) {
                characterPos = {
                    Pavo: {x: 1650, y: 790},
                }
            }
            if (isBrowser) {
                characterPos = {
                    Pavo: {x: 1700, y: 800},
                }
            }

            this.setState({
                characters: [
                    {component: Background, x: 0, y: 0, scale: 1, timeout: 0, background: grownUpBackground},
                    {component: Construction, x: 1040, y: 450, scale: 1, timeout: 0, background: ''},
                    {
                        component: Texto,
                        x: textXPos - 5,
                        y: 415,
                        color: 0x0D3157,
                        fontSize:fontSize,
                        scale: 1,
                        timeout: 0,
                        background: this.props.dictionary[this.props.options.lang].under_construction
                    },
                    {
                        component: Texto,
                        x: textXPos,
                        y: 410,
                        color: 0xE3E3E3,
                        fontSize:fontSize,
                        scale: 1,
                        timeout: 0,
                        background: this.props.dictionary[this.props.options.lang].under_construction
                    },
                    // {component: Texto, x: textXPos, y: 470, scale: 1, timeout: 0, background: this.props.dictionary[this.props.options.lang].under_construction},
                    {component: ConejoBebe, x: 500, y: 820, scale: .35, timeout: 0, background: ''},
                    {component: ConejoMama, x: 300, y: 820, scale: .35, timeout: 0, background: ''},
                    {component: Background, x: 0, y: 880, scale: 1, timeout: 0, background: grownUpsGrass},
                    {
                        component: Pavo,
                        x: characterPos.Pavo.x,
                        y: characterPos.Pavo.y,
                        scale: 0.7,
                        timeout: 0,
                        background: ''
                    },
                    {component: Background, x: -50, y: 930, scale: .6, timeout: 0, zOrder: 2, background: yuyo},
                    {component: Background, x: 400, y: 910, scale: .6, timeout: 0, zOrder: 2, background: yuyo},
                    {component: CortinaReverse, x: 960, y: 550, scale: 1.2, timeout: 0, zOrder: 10, background: ''},
                ]
            })

        }, 2500);
        this.props.app._options.backgroundColor = parseInt(this.props.color.replace(/^#/, ''), 16);
    }
    loadResources(unprocessedResources) {

        var binaryOptions = {
            loadType: PIXI.loaders.Resource.LOAD_TYPE.XHR,
            xhrType: PIXI.loaders.Resource.XHR_RESPONSE_TYPE.BUFFER
        };

        for (var _i = 0, _a = unprocessedResources; _i < _a.length; _i++) {
            var resource = _a[_i];

            try {
                for (var property1 in this._resources) {
                    console.log(property1);
                }


                PIXI.loader.add(resource, resource);
            }
            catch (error) {
                // console.error(error);
            }

        }
        PIXI.loader.once("complete", (loader, resources) => {
            this._resources = resources;
            this.loadCharacters();

            this.transitionTimeOut = setTimeout(()=>{
                this.props.actions.hideTransition();
            },3000)

        });
        PIXI.loader.load();
    };


    componentWillUnmount() {

        this.props.app.stage.removeChildren();

        if(this.mountTimeOut){
            clearTimeout(this.mountTimeOut);
        }

        if(this.transitionTimeOut){
            clearTimeout(this.transitionTimeOut);
        }

    }


    shouldComponentUpdate(nextProps, nextState) {
        if (this.state === nextState) {
            return false;
        }

        return true
    }


    render() {
        const {options, dictionary} = this.props;

        return (

            <Container>
                {this.state.characters.map((t, index) => {
                    const TagName = t.component;
                    // console.log(this.props.app);
                    return <TagName x={t.x} key={"char" + index} y={t.y} scale={t.scale} color={t.color} fontSize={t.fontSize} timeout={t.timeout}
                                    background={t.background} app={this.props.app}/>
                })
                }

            </Container>
        )
    }
}

GrownUps.propTypes = {
    options: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    return {
        options: state.options,
        dictionary: state.dictionary,

    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...actions}, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(GrownUps)