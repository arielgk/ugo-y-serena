import {Sprite, Text, ParticleContainer} from '@inlet/react-pixi'
import React from 'react';
import cortina_ske from '../../resource/burbujas/burbujas-2_ske.json';
import cortina_tex from '../../resource/burbujas/burbujas-2_tex.json';
import cortina_png from '../../resource/burbujas/burbujas-2_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class CortinaReverse extends React.Component {

    constructor(props) {
        super(props);

    }

    componentDidMount() {
        this.props.app.ticker.add(this.tick)

    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)

    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(cortina_ske);
        factory.parseTextureAtlasData(cortina_tex, PIXI.Texture.fromImage(cortina_png));

        let armatureDisplay = factory.buildArmatureDisplay("Armature", "burbujas-2");
        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;

        const animationConfig = armatureDisplay.animation.animationConfig;
        animationConfig.name = "burbujas-2"; // Animation state name.
        animationConfig.animation = "Animation2"; // Animation name.
        animationConfig.playTimes = 1; // Play one time.
        animationConfig.timeScale = 0.6; // Play speed.
        animationConfig.duration = 3.0; // Interval play.
        armatureDisplay.animation.playConfig(animationConfig);

        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);
        return null
    }
}

export default CortinaReverse;
