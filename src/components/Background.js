import React from 'react';
import {Sprite} from '@inlet/react-pixi';
import * as PIXI from "pixi.js";

class Background extends React.Component {

    render() {


        let sprite = new PIXI.Sprite.fromImage(this.props.background);

        sprite.x = this.props.x;
        sprite.y = this.props.y;

        sprite.scale.x = this.props.scale;
        sprite.scale.y = this.props.scale;

        setTimeout(() => {
            this.props.app.stage.addChild(sprite);
        }, this.props.timeout);

        return null

    }
}

export default Background;
