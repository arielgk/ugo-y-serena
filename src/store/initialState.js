const baseUrl = process.env.NODE_ENV === 'production' ? '//preview.ugoyserena.com' : 'http://localhost:3000';
export default {
    ajaxCallInProgress: 0,
    transition:true,
    chapterVisibility:false,
    options: {
        baseUrl: baseUrl,
        lang: "es",


        colors: {
            home: [
                '#ff4247',
                '#ff6d6c'
            ],
            videogame: [
                '#f5ce2d',
                '#f4c303'
            ],
            games: [
                '#5de550',
                '#53df20'
            ],
            chapters: [
                '#5ec5f0',
                '#4eb9ee'
            ],
            grownups: [
                '#9d48d3',
                '#8428c9'
            ],

        }

    },
    // langs: [
    //     {
    //         slug: "es",
    //         label: "Español",
    //
    //     },
    //     {
    //         slug: "en",
    //         label: "Ingles",
    //     }
    // ],
    dictionary: {
        es: {
            es: 'Esp',
            en: 'Ing',
            pt: 'Por',
            loading: 'Cargando...',
            home: 'Home',
            video_game: 'Video Juego',
            more_games: 'Más Juegos',
            chapters: 'Capítulos',
            grown_ups: 'Adultos',
            rotate_device:'Gira tu dispositivo',
            under_construction:'EN CONSTRUCCIÓN',
        },
        en: {
            es: 'Spa',
            en: 'Eng',
            pt: 'Por',
            loading: 'Loading...',
            home: 'Home',
            video_game: 'Video Game',
            more_games: 'More Games',
            chapters: 'Chapters',
            grown_ups: 'Grown Ups',
            rotate_device:'Rotate your device',
            under_construction:'UNDER CONSTRUCTION',
        },
        pt: {
            es: 'Esp',
            en: 'Ing',
            pt: 'Por',
            loading: 'Carregando...',
            home: 'Home',
            video_game: 'Videogame',
            more_games: 'Mais jogos',
            chapters: 'Capítulos',
            grown_ups: 'Adultos',
            rotate_device:'Rodar o seu dispositivo',
            under_construction:'EM CONSTRUCÃO',
        }
    },
    chapters: [
        {
            id: '01.jpg',
            es: 'A Ugo le da miedo dormir en la oscuridad',
            en: 'Ugo is afraid of sleeping in darkness',
            pt: 'Ugo tem medo de dormir no escuro',
        },  {
            id: '02.jpg',
            es: 'Ugo odia comer brócolis',
            en: 'hates to eat broccoli',
            pt: 'Ugo detesta brócolis',
        },  {
            id: '03.jpg',
            es: 'A Ugo le encanta pintar... las paredes de su casa',
            en: 'Ugo loves to paint... the living room walls!',
            pt: 'Ugo adora pintar... as paredes da sua casa',
        },  {
            id: '04.jpg',
            es: 'Ugo no quiere ordenar su cuarto',
            en: 'Ugo doesn\'t want to tidy up his bedroom',
            pt: 'Ugo nao quer arrumar o quarto',
        },  {
            id: '05.jpg',
            es: 'Ugo le tiene miedo a los truenos',
            en: 'Ugo is afraid of thunders',
            pt: 'Ugo tem medo de trovão',
        },  {
            id: '06.jpg',
            es: 'Ugo no quiere levantarse temprano',
            en: 'Ugo doesn\'t like to wake up early',
            pt: 'Ugo nao quer acordar cedo',
        },  {
            id: '07.jpg',
            es: 'Ugo quiere su regalo de cumpleaños YA!!',
            en: 'Ugo wants his birthday present now!',
            pt: 'Ugo quer agora seu presente!',
        },  {
            id: '08.jpg',
            es: 'Ugo se comió todo el chocolate!',
            en: 'Ugo ate the whole chocolate',
            pt: 'Ugo comeu todo o chocolate!',
        },  {
            id: '09.jpg',
            es: 'Ugo se da un baño de burbujas',
            en: 'Ugo takes a bubble bath',
            pt: 'Ugo toma banho de bolhas de sabão',
        },  {
            id: '10.jpg',
            es: 'Ugo se anima a andar en bici sin rueditas',
            en: 'Ugo dares to ride his bike without training wheels',
            pt: 'Ugo se anima a andar de bicicleta sem rodinhas',
        },
    ]

}
