import React from 'react';
import bubble_ske from '../../resource/buble_single/buble_single_ske.json';
import bubble_tex from '../../resource/buble_single/buble_single_tex.json';
import bubble_png from '../../resource/buble_single/buble_single_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class BubbleSingle extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(bubble_ske);

        factory.parseTextureAtlasData(bubble_tex, PIXI.Texture.fromImage(bubble_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "buble_single");
        armatureDisplay.animation.play("animtion0");
        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        armatureDisplay.animation.timeScale = .5;
        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);

        return null
    }
}

export default BubbleSingle;
