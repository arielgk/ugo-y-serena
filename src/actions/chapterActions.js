import * as actiontypes from './actionTypes';

export const hideChapters = () => {

    return (dispatch) => {
        dispatch({type: actiontypes.HIDE_CHAPTERS});

    }
}


export const showChapters = () => {

    return (dispatch) => {
        dispatch({type: actiontypes.SHOW_CHAPTERS});

    }
}