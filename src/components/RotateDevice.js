import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';



class RotateDevice extends React.Component{


    render(){
        const { options,dictionary} = this.props;
        return(
            <div className="rotateDevice">
                <div>
                    {dictionary[options.lang].rotate_device}
                </div>

            </div>
        )
    }
}
RotateDevice.propTypes = {
    options: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,

}
const mapStateToProps = (state, ownProps) => {
    return {
        options: state.options,
        dictionary: state.dictionary,

    }
}


export default connect(mapStateToProps)(RotateDevice)