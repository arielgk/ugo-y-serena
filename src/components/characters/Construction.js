import {Sprite, Text} from '@inlet/react-pixi'
import PropTypes from 'prop-types';
import React from 'react';
import construction_ske from '../../resource/construction/construccion_ske.json';
import construction_tex from '../../resource/construction/construccion_tex.json';
import construction_png from '../../resource/construction/construccion_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";
import {connect} from 'react-redux';

class Construction extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(construction_ske);
        factory.parseTextureAtlasData(construction_tex, PIXI.Texture.fromImage(construction_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "construccion");
        armatureDisplay.animation.play("animtion0");
        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);
        return null
    }
}

Construction.propTypes = {
    options: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,

}

const mapStateToProps = (state, ownProps) => {
    return {
        options: state.options,
        dictionary: state.dictionary,

    }
}

export default connect(mapStateToProps)(Construction);
