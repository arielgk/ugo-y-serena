import {Sprite, Text} from '@inlet/react-pixi'
import React from 'react';
import globo_ske from '../../resource/globo/globo_ske.json';
import globo_tex from '../../resource/globo/globo_tex.json';
import globo_png from '../../resource/globo/globo_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class Globo extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(globo_ske);
        factory.parseTextureAtlasData(globo_tex, PIXI.Texture.fromImage(globo_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "globo");

        armatureDisplay.animation.play("timeline");

        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        armatureDisplay.animation.timeScale = 1.1;
        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);
        return null
    }
}

export default Globo;
