import {Sprite, Text} from '@inlet/react-pixi'
import React from 'react';
import serena_ske from '../../resource/serenaHome2/serena_ske.json';
import serena_tex from '../../resource/serenaHome2/serena_tex.json';
import serena_png from '../../resource/serenaHome2/serena_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class SerenaHome extends React.Component {

    constructor(props) {
        super(props);

    }

    componentDidMount() {

        this.props.app.ticker.add(this.tick)

    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state === nextState) {
            return false;
        }
        return true
    }

    render() {

        //

        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(serena_ske);
        factory.parseTextureAtlasData(serena_tex, PIXI.Texture.fromImage(serena_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "serena");

        armatureDisplay.animation.play("timeline");

        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.rotation = 0;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        armatureDisplay.animation.timeScale = .8;

        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
            // this.props.ready();
        }, this.props.timeout);

        return null

    }
}

export default SerenaHome;
