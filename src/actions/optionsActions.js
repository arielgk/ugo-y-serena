import * as actiontypes from './actionTypes';


export const setLanguage =(lang)=>{
    return (dispatch )=>{
        dispatch({type:actiontypes.SET_LANGUAGE, lang:lang});

    }
}
