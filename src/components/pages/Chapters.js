import React from 'react';
import PropTypes from 'prop-types';
import Cangrejo from '../characters/Cangrejo';
import Globo from '../characters/Globo';
import {Container} from '@inlet/react-pixi';
import Background from '../Background';
import chaptersBackground from '../../resource/ui/chaptersBackground.jpg';
import PezA from "../characters/pez_a";
import PezB from "../characters/pez_b";
import PezV from "../characters/pez_v";
import CortinaReverse from "../characters/CortinaReverse";
import BubbleSingle from "../characters/BubbleSingle";
import {isBrowser, isMobile, isTablet} from "react-device-detect";
import * as PIXI from "pixi.js";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as transitionActions from "../../actions/transitionActions";
import * as chapterActions from "../../actions/chapterActions";
import globo_png from '../../resource/globo/globo_tex.png';
import cangrejo_png from '../../resource/cangrejo/cangrejo_tex.png';
import pez_a_png from '../../resource/pez_a/pez_a_tex.png';
import pez_b_png from '../../resource/pez_b/p_b_tex.png';
import pez_v_png from '../../resource/pez_v/p_violeta_tex.png';
import bubble_png from '../../resource/buble_single/buble_single_tex.png';
import cortina_png from '../../resource/burbujas/burbujas-2_tex.png';

class Chapters extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            characters: [],
        }

        this._resources = [];
        this.mountTimeOut = null;
        this.chapterTimeout = null;
        this.transitionTimeOut = null;
        this.app = this.props.app;
        this.loadCharacters = this.loadCharacters.bind(this);
        this.loadResources = this.loadResources.bind(this);

        const resources = []
        resources.push(cortina_png, chaptersBackground, globo_png, cangrejo_png, pez_v_png, pez_b_png, pez_a_png, bubble_png);

        this.loadResources(resources)
    }

    componentDidMount() {

    }

    loadResources(unprocessedResources) {

        var binaryOptions = {
            loadType: PIXI.loaders.Resource.LOAD_TYPE.XHR,
            xhrType: PIXI.loaders.Resource.XHR_RESPONSE_TYPE.BUFFER
        };

        for (var _i = 0, _a = unprocessedResources; _i < _a.length; _i++) {
            var resource = _a[_i];

            try {
                for (var property1 in this._resources) {
                    console.log(property1);
                }

                PIXI.loader.add(resource, resource);
            }
            catch (error) {
                // console.error(error);
            }

        }
        PIXI.loader.once("complete", (loader, resources) => {
            this._resources = resources;
            this.loadCharacters();



            this.transitionTimeOut = setTimeout(() => {
                this.props.actions.hideTransition();
            }, 3000)

            this.chapterTimeout = setTimeout(() => {
                this.props.actions.showChapters();
            }, 8000);

        });
        PIXI.loader.load();
    };

    loadCharacters() {

        this.mountTimeOut = setTimeout(() => {

            let characterPos = {
                Globo: {x: 300, y: 500},
                Cangrejo: {x: 1700, y: 890},
            }
            if (isMobile) {
                characterPos = {
                    Globo: {x: 300, y: 500},
                    Cangrejo: {x: 1700, y: 800},
                }
            }
            if (isTablet) {
                characterPos = {
                    Globo: {x: 200, y: 500},
                    Cangrejo: {x: 1750, y: 900},
                }
            }
            if (isBrowser) {
                characterPos = {
                    Globo: {x: 230, y: 500},
                    Cangrejo: {x: 1700, y: 900},
                }
            }

            this.setState({
                characters: [
                    {component: Background, x: 0, y: 0, scale: 1, timeout: 0, background: chaptersBackground},
                    {
                        component: Cangrejo,
                        x: characterPos.Cangrejo.x,
                        y: characterPos.Cangrejo.y,
                        scale: .5,
                        timeout: 0,
                        background: ''
                    },
                    {component: BubbleSingle, x: 300, y: 400, scale: 1, timeout: 0, background: ''},
                    {
                        component: Globo,
                        x: characterPos.Globo.x,
                        y: characterPos.Globo.y,
                        scale: .35,
                        timeout: 0,
                        background: ''
                    },

                    {component: PezA, x: 1750, y: 250, scale: .13, timeout: 0, background: ''},
                    {component: PezV, x: 1750, y: 350, scale: .25, timeout: 0, background: ''},
                    {component: PezB, x: 1750, y: 500, scale: .06, timeout: 0, background: ''},
                    {component: CortinaReverse, x: 960, y: 550, scale: 1.2, timeout: 0, zOrder: 10, background: ''},
                ]
            })
            this.props.showChapterModal(true);
        }, 2500);

        this.props.app._options.backgroundColor = parseInt(this.props.color.replace(/^#/, ''), 16);
    }

    componentWillUnmount() {
        this.props.app.stage.removeChildren();

        if (this.mountTimeOut) {
            clearTimeout(this.mountTimeOut);
        }

        if (this.chapterTimeout) {
            clearTimeout(this.chapterTimeout);
        }

        if(this.transitionTimeOut){
            clearTimeout(this.transitionTimeOut);
        }

        this.props.actions.hideChapters();


    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state === nextState) {
            return false;
        }

        return true
    }

    render() {

        return (

            <Container>
                {this.state.characters.map((t, index) => {
                    const TagName = t.component;

                    return <TagName x={t.x} key={"char" + index} y={t.y} scale={t.scale} timeout={t.timeout}
                                    background={t.background} app={this.props.app}/>
                })
                }
            </Container>
        )
    }
}

Chapters.propTypes = {
    options: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,

}

const mapStateToProps = (state, ownProps) => {
    return {
        options: state.options,
        dictionary: state.dictionary,

    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators(Object.assign({}, transitionActions, chapterActions), dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Chapters)