import React from 'react';
import conejo_mama_ske from '../../resource/conejo_mama/conejomama_ske.json';
import conejo_mama_tex from '../../resource/conejo_mama/conejomama_tex.json';
import conejo_mama_png from '../../resource/conejo_mama/conejomama_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class ConejoMama extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(conejo_mama_ske);
        factory.parseTextureAtlasData(conejo_mama_tex, PIXI.Texture.fromImage(conejo_mama_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "conejomama");

        armatureDisplay.animation.play("timeline");

        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        armatureDisplay.animation.timeScale = 1;
        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);
        return null
    }
}

export default ConejoMama;
