import {Sprite, Text} from '@inlet/react-pixi'
import React from 'react';
import cinta_ske from '../../resource/cinta/cinta_ske.json';
import cinta_tex from '../../resource/cinta/cinta_tex.json';
import cinta_png from '../../resource/cinta/cinta_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class Cinta extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(cinta_ske);

        factory.parseTextureAtlasData(cinta_tex, PIXI.Texture.fromImage(cinta_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "cinta");
        armatureDisplay.animation.play("animtion0");
        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);
        console.log(this.props.app.stage);

        return null
    }
}

export default Cinta;
