import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Root from './components/Root';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store/configureStore';
import initialState from './store/initialState';
import { setLanguage} from "./actions/optionsActions";


// const canvasElement = document.getElementById("container")
// const app = new PIXI.Application(800, 600, {
//     backgroundColor: 0x10bb99,
//     view: canvasElement
// });
//
// const addChildren = (el) => {
//
//     // console.log(app.stage.addChild(el));
//     // app.stage.addChild(el);
//     app.stage.addChild(el);
//     console.log(app.stage);
// }
//
// console.log(app.ticker);
// app.ticker.start();
// // app.ticker.shared.add(app.rederer, app);
//
// render(
//     <Container>
//         <BunnyStage addChildren={addChildren}></BunnyStage>
//     </Container>
//     ,
//
//     app.stage
// );
//




const store = configureStore(initialState)

// console.log(window.location.pathname);


if(window.location.pathname.startsWith('/en') ){
    store.dispatch(setLanguage('en'));
}else if (window.location.pathname.startsWith('/es')) {
    store.dispatch(setLanguage('es'));

}else{

    store.dispatch(setLanguage('pt'));
}

if(window.location.pathname=== '/'){
    window.location.href = "/es";

}

ReactDOM.render(<Root  store={store}/>, document.getElementById('root'));
registerServiceWorker();
