import React from 'react';

import {Sprite, Stage, render, Container} from "react-pixi-fiber";
import bunny from "../bunny.png";
import * as PIXI from "pixi.js";
import dragonBones from '../dragonBones';

import ugo_ske from '../resource/ugo/ugobbbb_ske.json';
import ugo_tex from '../resource/ugo/ugobbbb_tex.json';
import ugo_png from '../resource/ugo/ugobbbb_tex.png';

console.log(dragonBones);
const Bunny = ({props}) => {

}

class BunnyStage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            resources: [
                "resource/ugo/ugobbbb_ske.json",

                "resource/ugo/ugobbbb_tex.json",
                "resource/ugo/ugobbbb_tex.png",
            ]
        }

    }

    componentDidMount() {
        console.log(this.state.resources);

    }

    render() {

        const factory = dragonBones.PixiFactory.factory;

        console.log(PIXI.Texture.fromImage(ugo_png));
        factory.parseDragonBonesData(ugo_ske);
        factory.parseTextureAtlasData(ugo_tex, PIXI.Texture.fromImage(ugo_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "ugobbbb");

        armatureDisplay.animation.play("animtion0");

        armatureDisplay.x = 200.0;
        armatureDisplay.y = 200.0;
        armatureDisplay.scale.x = .1;
        armatureDisplay.debugDraw = true;
        armatureDisplay.scale.y = .1;
        console.log(this);

        this.props.addChildren(armatureDisplay);
        return (
            <Container></Container>

        );
    }

}

export default BunnyStage;
