import React from 'react';
import PropTypes from 'prop-types';
import Ugo from '../characters/Ugo';
import Serena from '../characters/SerenaHome';
import {Container} from '@inlet/react-pixi';

import CortinaReverse from "../characters/CortinaReverse";
import Background from '../Background';
import homeBackground from '../../resource/ui/homeBackgroundLow.jpg';
import ballenita from '../../resource/ballenita/ballenita.png';
import bici from '../../resource/bici/bici.png';
import pelota from '../../resource/pelota/pelota.png';
import * as PIXI from "pixi.js";
import cortina_png from '../../resource/burbujas/burbujas-2_tex.png';

import serena_png from '../../resource/serenaHome2/serena_tex.png';
import ugo_png from '../../resource/ugo/ugo_tex.png';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as actions from "../../actions/transitionActions";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            characters: [],
            isMounted: this.props.isMounted,
            alpha: 0,
            show: false,

        }

        this._resources = [];

        this.mountTimeOut = null;
        this.transitionTimeOut = null;
        this.ready = this.ready.bind(this);
        this.loadCharacters = this.loadCharacters.bind(this);
        this.loadResources = this.loadResources.bind(this);

        const resources = []
        resources.push(cortina_png, serena_png, ugo_png, homeBackground, ballenita, bici, pelota);

        this.loadResources(resources)

    }

    loadResources(unprocessedResources) {

        var binaryOptions = {
            loadType: PIXI.loaders.Resource.LOAD_TYPE.XHR,
            xhrType: PIXI.loaders.Resource.XHR_RESPONSE_TYPE.BUFFER
        };

        for (var _i = 0, _a = unprocessedResources; _i < _a.length; _i++) {
            var resource = _a[_i];

            try {
                // for (var property1 in this._resources) {
                //
                // }

                PIXI.loader.add(resource, resource);
            }
            catch (error) {
                // console.error(error);
            }

        }
        PIXI.loader.once("complete", (loader, resources) => {
            this._resources = resources;
            console.log(this._resources);

            this.loadCharacters();


            this.transitionTimeOut = setTimeout(()=>{
                this.props.actions.hideTransition();
            },3000);
        });
        PIXI.loader.load();
    };

    ready() {
        this.setState({
            alpha: 0,
        });

        console.log('ready');
    }

    componentDidMount() {
        this.props.app._options.backgroundColor = parseInt(this.props.color.replace(/^#/, ''), 16);

    }

    loadCharacters() {

        this.mountTimeOut = setTimeout(() => {

            this.setState({
                characters: [

                    {component: Background, x: 0, y: 0, scale: 1, timeout: 0, zOrder: 10, background: homeBackground},
                    {component: Ugo, x: 750, y: 600, scale: .22, timeout: 0, zOrder: 10, background: ''},
                    {component: Background, x: 770, y: 550, scale: .55, timeout: 0, zOrder: 1, background: ballenita},
                    {component: Serena, x: 1900, y: 450, scale: 1.2, timeout: 0, zOrder: -1, background: ''},
                    {component: Background, x: -300, y: 830, scale: 1, timeout: 0, zOrder: 2, background: pelota},
                    {component: Background, x: 1250, y: 680, scale: 1, timeout: 0, zOrder: 2, background: bici},
                    {component: CortinaReverse, x: 960, y: 550, scale: 1.2, timeout: 0, zOrder: 10, background: ''},
                ]
            })
        }, 2500);

    }

    componentWillUnmount() {

        this.props.app.stage.removeChildren();

        if(this.mountTimeOut){
            clearTimeout(this.mountTimeOut);
        }

        if(this.transitionTimeOut){
            clearTimeout(this.transitionTimeOut);
        }

    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state === nextState) {
            return false;
        }
        // if(this.props === nextProps){
        //     return false
        // }

        return true
    }

    componentDidUpdate() {

    }

    render() {

        return (

            <Container delayTime={2000}>
                {this.state.characters.map((t, index) => {
                    const TagName = t.component;

                    return <TagName x={t.x} key={"char" + index} y={t.y} scale={t.scale} timeout={t.timeout}
                                    background={t.background} app={this.props.app} ready={this.ready}
                                    resources={this._resources} alpha={this.state.alpha}/>
                })
                }


            </Container>

        )
    }
}

Home.propTypes = {
    options: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,

}

const mapStateToProps = (state, ownProps) => {
    return {
        options: state.options,
        dictionary: state.dictionary,

    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...actions}, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)