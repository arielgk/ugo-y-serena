
import React from 'react';

export const delayUnmounting = (Component) => {
    return class extends React.Component {
        state = {
            shouldRender: this.props.isMounted
        };

        componentDidUpdate(prevProps) {

            setTimeout(
                () => this.setState({shouldRender: true}),
                this.props.delayTime

            );
        }

        render() {
            console.log(this.props);
            return this.state.shouldRender ? <Component {...this.props} /> : null;
        }
    };
}