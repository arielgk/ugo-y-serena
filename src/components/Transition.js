import React from 'react';
// import PropTypes from 'prop-types';
import * as PIXI from "pixi.js";
import {Container, Sprite} from '@inlet/react-pixi';
import Cortina from "./characters/Cortina";
import cortina_png from '../resource/burbujas/burbujas-2_tex.png';

class Transition extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            characters: []
        }

        this._resources = [];
        this.mountTimeOut = null;

        this.app = this.props.app;
        this.loadCharacters = this.loadCharacters.bind(this);
        this.loadResources = this.loadResources.bind(this);

        const resources = []
        resources.push(cortina_png);
        this.loadResources(resources)

    }

    loadResources(unprocessedResources) {

        const loader = new PIXI.loaders.Loader();

        var binaryOptions = {
            loadType: PIXI.loaders.Resource.LOAD_TYPE.XHR,
            xhrType: PIXI.loaders.Resource.XHR_RESPONSE_TYPE.BUFFER
        };

        for (var _i = 0, _a = unprocessedResources; _i < _a.length; _i++) {
            var resource = _a[_i];

            try {
                for (var property1 in this._resources) {
                    console.log(property1);
                }

                loader.add(resource, resource);
            } catch (error) {
                // console.error(error);
            }

        }
        loader.once("complete", (loader, resources) => {
            this._resources = resources;
            console.log(this._resources);

            this.loadCharacters();

        });
        loader.load();
    };

    loadCharacters() {
        this.mountTimeOut = setTimeout(() => {
            this.setState({
                characters: [

                    {component: Cortina, x: 960, y: 550, scale: 1.2, timeout: 0, zOrder: 10, background: ''},

                ]
            })
        }, 0);

    }

    componentDidMount() {

    }

    componentWillUnmount() {
        console.log('unmount');

        this.props.app.stage.removeChildren();
        clearTimeout(this.mountTimeOut);

    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state === nextState) {
            return false;
        }

        return true
    }

    render() {
        console.log(this.props.app);
        console.log('render transition');

        return <Container>
            {this.state.characters.map((t, index) => {
                const TagName = t.component;

                console.log(this.props.app);
                return <TagName x={t.x} key={"char" + index} y={t.y} scale={t.scale} timeout={t.timeout}
                                background={t.background} app={this.props.app}/>
            })
            }

        </Container>

    }
}

export default Transition
