import {Sprite, Text} from '@inlet/react-pixi'
import React from 'react';
import ugo_ske from '../../resource/ugo/ugo_ske.json';
import ugo_tex from '../../resource/ugo/ugo_tex.json';
import ugo_png from '../../resource/ugo/ugo_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class Ugo extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(ugo_ske);
        factory.parseTextureAtlasData(ugo_tex, PIXI.Texture.fromImage(ugo_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "ugo");

        armatureDisplay.animation.play("animtion0");

        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.animation.timeScale = 1.2;
        armatureDisplay.scale.y = this.props.scale;

        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);
        return null
    }
}

export default Ugo;
