import React from 'react';
import cortina_ske from '../../resource/burbujas/burbujas-2_ske.json';
import cortina_tex from '../../resource/burbujas/burbujas-2_tex.json';
import cortina_png from '../../resource/burbujas/burbujas-2_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class Cortina extends React.Component {

    constructor(props) {
        super(props);

    }

    componentDidMount() {
        this.props.app.ticker.add(this.tick)

    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)

    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(cortina_ske);
        factory.parseTextureAtlasData(cortina_tex, PIXI.Texture.fromImage(cortina_png));

        let armatureDisplay = factory.buildArmatureDisplay("Armature", "burbujas-2");

        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;

        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;

        const animationConfig = armatureDisplay.animation.animationConfig;
        animationConfig.name = "test"; // Animation state name.
        animationConfig.animation = "animation1"; // Animation name.
        animationConfig.playTimes = 1; // Play one time.
        animationConfig.timeScale = 0.5; // Play speed.
        animationConfig.duration = 3.0; // Interval play.

        armatureDisplay.animation.playConfig(animationConfig);

        setTimeout(() => {

            this.props.app.stage.addChild(armatureDisplay);

        }, 0);
        return null
    }
}

export default Cortina;
