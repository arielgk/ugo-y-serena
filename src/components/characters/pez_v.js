import React from 'react';
import pez_ske from '../../resource/pez_v/p_violeta_ske.json';
import pez_tex from '../../resource/pez_v/p_violeta_tex.json';
import pez_png from '../../resource/pez_v/p_violeta_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class PezV extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(pez_ske);
        factory.parseTextureAtlasData(pez_tex, PIXI.Texture.fromImage(pez_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "p_violeta");
        armatureDisplay.animation.play("animtion0");
        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        armatureDisplay.animation.timeScale = 1.2;
        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);
        return null
    }
}

export default PezV;
