import {combineReducers} from 'redux';
import ajaxCallInProgress from "./ajaxStatusReducer";
import dictionary from "./dictionaryReducer";
import chapters from "./chaptersReducer";
import transition from './transitionReducer';
import chapterVisibility from './chapterVisibilityReducer';
import options from './optionsReducer';

const rootReducers = combineReducers({
    ajaxCallInProgress,
    dictionary,
    options,
    chapters,
    transition,
    chapterVisibility,
});

export default rootReducers;
