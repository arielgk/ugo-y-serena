import React, {Component} from 'react';
import PropTypes from 'prop-types';
import '../App.css';

import Home from './pages/Home';
import Games from './pages/Games';
import Chapters from "./pages/Chapters";
import ChapterSlider from './pages/parts/ChapterSlider';
import VideoGame from "./pages/VideoGame";
import GrownUps from "./pages/GrownUps";

import Transition from './Transition';
import withSizes from 'react-sizes'

import {Stage, Container, Provider as PixiProvider, Sprite, Text} from '@inlet/react-pixi'
import logo from '../resource/ui/logo.png';

import HeaderBackgroundImage from '../components/ui/headerBackgoundImage';
import FooterBackgroundImage from '../components/ui/footerBackgroundImage';
import {Switch, Route, Link, NavLink} from 'react-router-dom';
import DelayLink from './DelayLink';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import * as optionActions from "../actions/optionsActions";
import * as transitionActions from "../actions/transitionActions";
import homeItem from '../resource/ui/home.png';
import videoGameItem from '../resource/ui/videogame.png';
import moreGamesItem from '../resource/ui/moregames.png';
import chaptersItem from '../resource/ui/chapters.png';
import grownUpsItem from '../resource/ui/grownups.png';

import eslang from '../resource/ui/lang-1.png';
import enlang from '../resource/ui/lang-2.png';
import ptlang from '../resource/ui/lang-3.png';
import RotateDevice from "./RotateDevice";



class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showChapters: false,
            showTransition: true,
            transitionInstances: 0,
            delayLink: 3000,
            imagesLoaded: false,
        };

        this.timeout = null;

        this.changeLang = this.changeLang.bind(this);
        this.showChapterModal = this.showChapterModal.bind(this);
        this.showTransition = this.showTransition.bind(this);
        this.runAfterImagesLoaded = this.runAfterImagesLoaded.bind(this);


    }

    changeLang(lang) {
        this.props.actions.setLanguage(lang);
    }

    showChapterModal(showChapter) {
        if (showChapter) {
            this.timeout = setTimeout(() => {

                this.setState({
                    showChapters: showChapter,
                })
            }, 3000);
        } else {
            this.setState({
                showChapters: showChapter,
            })
        }

    }

    runAfterImagesLoaded() {
        console.log('loaded');
        this.setState({
            imagesLoaded: true,
        })
    }

    showTransition(showTransition) {

        // this.setState({
        //     showTransition: showTransition,
        //     transitionInstances: showTransition ? 1 : 0,
        // })

        this.props.actions.showTransition();

    }

    componentWillUnmount() {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }

    }

    render() {


        const {options, dictionary, isMobile, isTablet,isTabletPortrait} = this.props;

        let path = 'home';
        if (window.location.pathname === '/' + options.lang + '/games') {
            path = 'games';
        } else if (window.location.pathname === '/' + options.lang + '/videogame') {
            path = 'videogame';
        } else if (window.location.pathname === '/' + options.lang + '/grown-ups') {
            path = 'grownups';
        } else if (window.location.pathname === '/' + options.lang + '/chapters') {
            path = 'chapters';
        } else {
            path = 'home';
        }

        const colors = options.colors[path];

        return (

            <div className="showContainer" style={{backgroundColor: colors[0]}}>


                <div className="headerContainer" style={{backgroundColor: colors[0]}}>
                    <div className="headerAspectRatio">
                        <HeaderBackgroundImage backColor={colors[0]} foreColor={colors[1]}/>

                    </div>
                    <div className="headerContent">
                        <div className="in-frame">


                            <div className="logoContainer">
                                <DelayLink delay={this.state.delayLink}
                                           exact='true'
                                           disabled={true}
                                           to={"/" + options.lang + "/"}
                                           onDelayStart={() => {
                                               this.showTransition(true)
                                               this.showChapterModal(false);
                                           }}

                                >
                                    <img className="logo" src={logo} alt="Ugo y Serena"/>
                                </DelayLink>
                            </div>
                            <div className="menuContainer">
                                <ul>
                                    <li className={window.location.pathname === '/' + options.lang + '/' ? 'active' : ''}>
                                        <DelayLink delay={this.state.delayLink}
                                                   exact='true'
                                                   disabled={true}
                                                   to={"/" + options.lang + "/"}
                                                   onDelayStart={() => {

                                                       this.showTransition(true)

                                                       this.showChapterModal(false);
                                                   }}

                                        >

                                            <img src={homeItem} alt=""/>
                                            <span>{dictionary[options.lang].home}</span>
                                        </DelayLink>
                                    </li>
                                    <li className={window.location.pathname === ('/' + options.lang + '/videogame') ? 'active' : ''}>
                                        <DelayLink delay={this.state.delayLink} exact='true'
                                                   to={"/" + options.lang + "/videogame"}
                                                   onDelayStart={() => {
                                                       this.showTransition(true)
                                                       this.showChapterModal(false);
                                                   }}>
                                            <img src={videoGameItem} alt=""/>
                                            <span>{dictionary[options.lang].video_game}</span>
                                        </DelayLink>
                                    </li>
                                    <li className={window.location.pathname === ('/' + options.lang + '/games') ? 'active' : ''}>
                                        <DelayLink delay={this.state.delayLink} exact='true'
                                                   to={"/" + options.lang + "/games"}

                                                   onDelayStart={() => {
                                                       this.showTransition(true)
                                                       this.showChapterModal(false);
                                                   }}>


                                            <img src={moreGamesItem} alt=""/>
                                            <span>{dictionary[options.lang].more_games}</span>
                                        </DelayLink>
                                    </li>
                                    <li className={window.location.pathname === ('/' + options.lang + '/chapters') ? 'active' : ''}>
                                        <DelayLink delay={this.state.delayLink} exact='true'
                                                   to={"/" + options.lang + "/chapters"}
                                                   onDelayStart={() => {
                                                       this.showTransition(true)
                                                   }}>

                                            <img src={chaptersItem} alt=""/>
                                            <span>{dictionary[options.lang].chapters}</span>


                                        </DelayLink>
                                    </li>
                                    <li className={window.location.pathname === ('/' + options.lang + '/grown-ups') ? 'active' : ''}>
                                        <DelayLink delay={this.state.delayLink} exact='true'
                                                   to={"/" + options.lang + "/grown-ups"}
                                                   onDelayStart={() => {
                                                       this.showTransition(true)
                                                       this.showChapterModal(false);
                                                   }}>


                                            <img src={grownUpsItem} alt=""/>
                                            <span>{dictionary[options.lang].grown_ups}</span>
                                        </DelayLink>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

                <div>
                    {this.props.transition ? <div className='transition'>
                            <Stage options={{
                                transparent: true,
                                width: 1920,
                                height: 1080
                            }}>
                                <Container>
                                    <PixiProvider>
                                        {app2 => {
                                            return <Transition showTransition={this.showTransition} app={app2}/>
                                        }
                                        }

                                    </PixiProvider>
                                </Container>
                            </Stage>

                        </div>
                        : ''}

                    <div className="canvasContainer">
                        <Stage options={{
                            backgroundColor: parseInt(colors[0].replace(/^#/, ''), 16),
                            width: 1920,
                            height: 1080
                        }}>
                            <Container style={this.state.imagesLoaded ? {display: 'block'} : {display: 'none'}}>
                                <PixiProvider>
                                    {app =>

                                        <Switch>
                                            <Route exact={true} path="/:lang/"
                                                   render={() => <Home app={app}
                                                                       color={colors[0]}/>}/>
                                            <Route exact={true} path="/:lang/games"
                                                   render={() => <Games app={app}
                                                                        isMounted={window.location.href.lastIndexOf("/") === 'games' ? true : ''}
                                                                        color={colors[0]}/>}/>
                                            <Route exact={true} path="/:lang/chapters"
                                                   render={() => <Chapters app={app} color={colors[0]}
                                                                           showChapterModal={this.showChapterModal}/>}/>
                                            <Route exact={true} path="/:lang/videogame"
                                                   render={() => <VideoGame app={app} color={colors[0]}/>}/>
                                            <Route exact={true} path="/:lang/grown-ups"
                                                   render={() => <GrownUps app={app} color={colors[0]}/>}/>


                                        </Switch>

                                    }
                                </PixiProvider>
                            </Container>
                        </Stage>

                        {this.props.chapterVisibility ? <ChapterSlider/> : ''}
                    </div>
                </div>

                <div className="footerContainer" style={{backgroundColor:colors[0]}}>
                    <div className="footerAspectRatio">
                        <FooterBackgroundImage backColor={colors[0]} foreColor={colors[1]}/>
                    </div>
                    <div className="footerContent">
                        <div className="in-frame">
                            <div className="lang-bar">
                                <ul>
                                    <li style={{
                                        backgroundImage: 'url(' + eslang + ')',
                                        backgroundRepeat: 'no-repeat',
                                        backgroundSize: '50px'
                                    }}>
                                        <NavLink exact
                                                 to={"/es/"}
                                                 onClick={() => {
                                                     this.changeLang('es');
                                                     this.showChapterModal(false);
                                                 }}>{dictionary[options.lang].es}</NavLink>
                                    </li>
                                    <li style={{
                                        backgroundImage: 'url(' + enlang + ')',
                                        backgroundRepeat: 'no-repeat',
                                        backgroundSize: '50px'
                                    }}>
                                        <NavLink exact
                                                 to={"/en/"}
                                                 onClick={() => {
                                                     this.changeLang('en')
                                                     this.showChapterModal(false);
                                                 }}>{dictionary[options.lang].en}</NavLink>
                                    </li>
                                    <li style={{
                                        backgroundImage: 'url(' + ptlang + ')',
                                        backgroundRepeat: 'no-repeat',
                                        backgroundSize: '50px'
                                    }}>
                                        <NavLink exact
                                                 to={"/pt/"}
                                                 onClick={() => {
                                                     this.changeLang('pt')
                                                     this.showChapterModal(false);
                                                 }}>{dictionary[options.lang].pt}</NavLink>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>

                </div>


                {isMobile  ? <RotateDevice/> : ""}


            </div>

        );
    }
}

App.propTypes = {
    options: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    transition: PropTypes.bool.isRequired,
    chapterVisibility:PropTypes.bool.isRequired,

}

const mapStateToProps = (state, ownProps) => {
    return {
        options: state.options,
        dictionary: state.dictionary,
        transition: state.transition,
        chapterVisibility: state.chapterVisibility,

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(Object.assign({}, optionActions, transitionActions), dispatch)
    }
}

const mapSizesToProps = ({width}) => ({
    isMobile: width < 480,
    isTablet: width < 1024,
    isTabletPortrait:width < 769 && width < 900,
})
export default connect(mapStateToProps, mapDispatchToProps)(withSizes(mapSizesToProps)(App));
