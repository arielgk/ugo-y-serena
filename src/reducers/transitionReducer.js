import * as actiontypes from '../actions/actionTypes';
import initialState from '../store/initialState';

const transition = (state = initialState.transition, action) => {

    switch (action.type) {
        case actiontypes.HIDE_TRANSITION:


            return false

        case actiontypes.SHOW_TRANSITION:
            return true
        default:
            return state
    }
}

export default transition;
