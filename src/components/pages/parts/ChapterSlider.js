import React from 'react';
import PropTypes from 'prop-types';
import withSizes from 'react-sizes'
import Swiper from 'react-id-swiper';
import {connect} from 'react-redux';
import ProgressiveImage from 'react-progressive-image';
import sliderBackground from '../../../resource/ui/sliderBackground.png';
class ChapterSlider extends React.Component {

    render() {

        const {chapters,options,isMobile,isTablet} = this.props
        let params = {};

        if(isTablet ){
            params = {
                slidesPerView: 1,
                slidesPerColumn: 1,
                spaceBetween: 30,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            };
        }else{
            params = {
                slidesPerView: 2,
                slidesPerColumn: 2,
                spaceBetween: 30,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            };
        }

        return (
            <div className="ChaptersModalContainer">
                <div className="sliderContainer" style={{ backgroundImage:"url("+sliderBackground+")", backgroundRepeat:'no-repeat', backgroundSize:"100% 100%"}}>
                    <Swiper {...params}>
                        {chapters.map((c, index) => {
                            return <div className="slide-item" key={c.id+'-'+index} >

                                <ProgressiveImage src={options.baseUrl+'/images/chapters/'+c.id}
                                                  placeholder={options.baseUrl+'/images/placeholder-chapter.jpg'}>
                                    {(src) => <img src={src} alt={c.en}/>}
                                </ProgressiveImage>



                                <p><b>E{index+1}:</b> {c[options.lang]}</p></div>
                        })}

                    </Swiper>
                </div>
            </div>

        )
    }

}

ChapterSlider.propTypes = {

    chapters: PropTypes.array.isRequired,
    options: PropTypes.object.isRequired,
}
const mapStateToProps = (state, ownProps) => {
    return {
        chapters: state.chapters,
        options:state.options,
    }
}

const mapSizesToProps = ({width}) => ({
    isMobile: width < 480,
    isTablet: width < 1024,
})

export default connect(mapStateToProps)(withSizes(mapSizesToProps)(ChapterSlider));


