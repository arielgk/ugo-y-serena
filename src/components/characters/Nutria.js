import {Sprite, Text} from '@inlet/react-pixi'
import React from 'react';
import nutria_ske from '../../resource/nutria/nutria_ske.json';
import nutria_tex from '../../resource/nutria/nutria_tex.json';
import nutria_png from '../../resource/nutria/nutria_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class Nutria extends React.Component {

    constructor(props) {
        super(props);
        this.timeOut = null;
        this._armatureDisplay = null;
    }

    state = {rotation: 0}

    componentDidMount() {

        this.props.app.ticker.add(this.tick)

    }

    componentDidUpdate() {

    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
        if (this.timeOut) {
            clearTimeout(this.timeOut);
        }
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(nutria_ske);
        factory.parseTextureAtlasData(nutria_tex, PIXI.Texture.fromImage(nutria_png));

        this._armatureDisplay = factory.buildArmatureDisplay("Armature", "nutria");

        this._armatureDisplay.x = this.props.x;
        this._armatureDisplay.y = this.props.y;
        this._armatureDisplay.scale.x = this.props.scale;
        this._armatureDisplay.debugDraw = false;
        this._armatureDisplay.scale.y = this.props.scale;
        this._armatureDisplay.animation.timeScale = 1.2;

        this._armatureDisplay.animation.play("timeline");

        this.timeOut = setTimeout(() => {
            this.props.app.stage.addChild(this._armatureDisplay);
        }, this.props.timeout);
        return null
    }
}

export default Nutria;
