import React from 'react';
import PropTypes from 'prop-types';
import UgoGame from '../characters/UgoGame';
import {Container} from '@inlet/react-pixi';
import Background from '../Background';
import videoGameBackground from '../../resource/ui/videoGameBackground.jpg';
import videoGameMidground from '../../resource/ui/videoGameMidground.png';
import SerenaPacman from "../characters/serenaPacman";
// import Cortina from "../characters/Cortina";
import CortinaReverse from "../characters/CortinaReverse";
import cortina_png from '../../resource/burbujas/burbujas-2_tex.png';
import serena_png from '../../resource/serenaPacman/serena_pacman_tex.png';
import ugoGame_png from '../../resource/ugoGame/ugo_game_tex.png';
// import Texto from "../Texto";
// import withSizes from 'react-sizes';
import {
    BrowserView,
    MobileView,
    isBrowser,
    isTablet,
    isMobile
} from "react-device-detect";

import * as PIXI from "pixi.js";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as actions from "../../actions/transitionActions";

class VideoGame extends React.Component {
    constructor(props) {
        super(props);

        this.mountTimeOut = null;
        this.transitionTimeOut = null;

        this.state = {
            characters: [],
            isMounted: true,
            count: 0,
        }

        this.app = this.props.app;

        this._resources = [];
        this.mountTimeOut = null;

        this.app = this.props.app;
        this.loadCharacters = this.loadCharacters.bind(this);
        this.loadResources = this.loadResources.bind(this);

        const resources = []
        resources.push(cortina_png,videoGameBackground, videoGameMidground,serena_png,ugoGame_png);

        this.loadResources(resources)

    }

    loadResources(unprocessedResources) {

        var binaryOptions = {
            loadType: PIXI.loaders.Resource.LOAD_TYPE.XHR,
            xhrType: PIXI.loaders.Resource.XHR_RESPONSE_TYPE.BUFFER
        };

        for (var _i = 0, _a = unprocessedResources; _i < _a.length; _i++) {
            var resource = _a[_i];

            try {
                for (var property1 in this._resources) {
                    console.log(property1);
                }

                PIXI.loader.add(resource, resource);
            }
            catch (error) {
                // console.error(error);
            }

        }
        PIXI.loader.once("complete", (loader, resources) => {
            this._resources = resources;
            this.loadCharacters();

            this.transitionTimeOut = setTimeout(()=>{
                this.props.actions.hideTransition();
            },3000)
        });
        PIXI.loader.load();
    };

    loadCharacters() {
        this.mountTimeOut = setTimeout(() => {

            let characterPos = {
                SerenaPacman: {x: 920, y: 600},
                UgoGame: {x: 1700, y: 890},
            }
            if (isMobile) {
                characterPos = {
                    SerenaPacman: {x: 920, y: 600},
                    UgoGame: {x: 1750, y: 690},
                }
            }
            if (isTablet) {
                characterPos = {
                    SerenaPacman: {x: 920, y: 600},
                    UgoGame: {x: 1750, y: 840},
                }
            }
            if (isBrowser) {
                characterPos = {
                    SerenaPacman: {x: 920, y: 600},
                    UgoGame: {x: 1700, y: 790},
                }
            }

            this.setState({
                characters: [
                    {component: Background, x: 0, y: 0, scale: 1, timeout: 0, background: videoGameBackground},
                    {
                        component: SerenaPacman,
                        x: characterPos.SerenaPacman.x,
                        y: characterPos.SerenaPacman.y,
                        scale: .5,
                        timeout: 0,
                        background: '',

                    },
                    {component: Background, x: 0, y: 0, scale: 1, timeout: 0, background: videoGameMidground,},
                    {
                        component: UgoGame,
                        x: characterPos.UgoGame.x,
                        y: characterPos.UgoGame.y,
                        scale: .5,
                        timeout: 0,
                        background: '',

                    },
                    {component: CortinaReverse, x: 960, y: 550, scale: 1.2, timeout: 0, zOrder: 10, background: ''},
                ],
            })
        }, 2500);
    }

    componentWillUnmount() {

        this.props.app.stage.removeChildren();

        if(this.mountTimeOut){
            clearTimeout(this.mountTimeOut);
        }

        if(this.transitionTimeOut){
            clearTimeout(this.transitionTimeOut);
        }

    }

    shouldComponentUpdate(nextProps, nextState) {

        if (this.state === nextState) {
            return false;
            // console.log('entro')
        }
        // console.log('entro')

        return true
    }

    render() {
        // const app = this.props.app;
        return (

            <Container options={{alpha: this.state.count === this.state.characters.length ? false : false}}>

                {

                    this.state.characters.map((t, index) => {
                        const TagName = t.component;

                        // console.log(this.props.app);
                        return <TagName x={t.x} key={"char" + index} y={t.y} scale={t.scale} timeout={t.timeout}
                                        background={t.background} app={this.app}/>
                    })

                }


            </Container>

        )
    }
}

VideoGame.propTypes = {
    options: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,

}

const mapStateToProps = (state, ownProps) => {
    return {
        options: state.options,
        dictionary: state.dictionary,

    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...actions}, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoGame)