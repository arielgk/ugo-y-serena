import {Sprite, Text} from '@inlet/react-pixi'
import React from 'react';
import penguin_ske from '../../resource/penguin/penguin_ske.json';
import penguin_tex from '../../resource/penguin/penguin_tex.json';
import penguin_png from '../../resource/penguin/penguin_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class Penguin extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(penguin_ske);
        factory.parseTextureAtlasData(penguin_tex, PIXI.Texture.fromImage(penguin_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "penguin");

        armatureDisplay.animation.play("tildar-carpeta");

        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        armatureDisplay.animation.timeScale = .7;
        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);

        return null
    }
}

export default Penguin;
