import React from 'react';
import serena_ske from '../../resource/serenaPacman/serena_pacman_ske.json';
import serena_tex from '../../resource/serenaPacman/serena_pacman_tex.json';
import serena_png from '../../resource/serenaPacman/serena_pacman_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class SerenaPacman extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(serena_ske);
        factory.parseTextureAtlasData(serena_tex, PIXI.Texture.fromImage(serena_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "serena_pacman");

        armatureDisplay.animation.play("animtion0");

        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        armatureDisplay.animation.timeScale = 1.2;
        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);

        }, this.props.timeout);

        return null
    }
}

export default SerenaPacman;
