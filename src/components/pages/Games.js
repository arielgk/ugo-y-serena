import React from 'react';
import PropTypes from 'prop-types';
import Penguin from '../characters/Penguin';
import Nutria from '../characters/Nutria';
import {Container} from '@inlet/react-pixi';
import Construction from '../characters/Construction';
import Background from '../Background';
import gamesBackground from '../../resource/ui/gamesBackground2.jpg';
import Texto from "../Texto";
import {connect} from 'react-redux';
import CortinaReverse from "../characters/CortinaReverse";
import Cinta from "../characters/Cinta";
import {isBrowser, isMobile, isTablet} from "react-device-detect";

import * as PIXI from "pixi.js";

import construction_png from '../../resource/construction/construccion_tex.png';
import penguin_png from '../../resource/penguin/penguin_tex.png';
import nutria_png from '../../resource/nutria/nutria_tex.png';
import cortina_png from '../../resource/burbujas/burbujas-2_tex.png';
import {bindActionCreators} from "redux";
import * as actions from "../../actions/transitionActions";

class Games extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            characters: [],
        }

        this._resources = [];
        this.mountTimeOut = null;

        this.app = this.props.app;
        this.loadCharacters = this.loadCharacters.bind(this);
        this.loadResources = this.loadResources.bind(this);

        const resources = []
        resources.push(cortina_png,gamesBackground, nutria_png, construction_png, penguin_png, CortinaReverse, Cinta);

        this.loadResources(resources)
    }

    componentDidMount() {

    }

    loadResources(unprocessedResources) {

        var binaryOptions = {
            loadType: PIXI.loaders.Resource.LOAD_TYPE.XHR,
            xhrType: PIXI.loaders.Resource.XHR_RESPONSE_TYPE.BUFFER
        };

        for (var _i = 0, _a = unprocessedResources; _i < _a.length; _i++) {
            var resource = _a[_i];

            try {
                for (var property1 in this._resources) {
                    console.log(property1);
                }

                PIXI.loader.add(resource, resource);
            }
            catch (error) {
                // console.error(error);
            }

        }
        PIXI.loader.once("complete", (loader, resources) => {
            this._resources = resources;
            this.loadCharacters();

            this.transitionTimeOut = setTimeout(()=>{
                this.props.actions.hideTransition();
            },3000)

        });
        PIXI.loader.load();
    };

    loadCharacters() {
        let textXPos = null;
        let fontSize = null;

        switch(this.props.options.lang){
            case 'es':
                textXPos = 690;
                fontSize = 60;
                break;
            case 'pt':
                textXPos = 720;
                fontSize = 60;
                break;

            default:

                textXPos = 680;
                fontSize = 50;

        }


        this.mountTimeOut = setTimeout(() => {

            let characterPos = {
                Nutria: {x: 400, y: 750},
                Penguin: {x: 1700, y: 900},
            }
            if (isMobile) {
                characterPos = {
                    Nutria: {x: 400, y: 750},
                    Penguin: {x: 1750, y: 900},
                }
            }
            if (isTablet) {
                characterPos = {
                    Nutria: {x: 400, y: 750},
                    Penguin: {x: 1650, y: 900},
                }
            }
            if (isBrowser) {
                characterPos = {
                    Nutria: {x: 400, y: 750},
                    Penguin: {x: 1660, y: 800},
                }
            }

            this.setState({
                characters: [
                    {component: Background, x: 0, y: 0, scale: 1, timeout: 0, background: gamesBackground},
                    {component: Construction, x: 1040, y: 450, scale: 1, timeout: 0, background: ''},
                    {
                        component: Texto,
                        x: textXPos - 5,
                        y: 415,
                        color: 0x0D3157,
                        fontSize:fontSize,
                        scale: 1,
                        timeout: 0,
                        background: this.props.dictionary[this.props.options.lang].under_construction
                    },
                    {
                        component: Texto,
                        x: textXPos,
                        y: 410,
                        color: 0xE3E3E3,
                        fontSize:fontSize,
                        scale: 1,
                        timeout: 0,
                        background: this.props.dictionary[this.props.options.lang].under_construction
                    },
                    {component: Cinta, x: 960, y: 725, scale: .7, timeout: 0, background: ''},
                    {
                        component: Nutria,
                        x: characterPos.Nutria.x,
                        y: characterPos.Nutria.y,
                        scale: .40,
                        timeout: 0,
                        background: ''
                    },
                    {
                        component: Penguin,
                        x: characterPos.Penguin.x,
                        y: characterPos.Penguin.y,
                        scale: .8,
                        timeout: 0,
                        background: ''
                    },
                    {component: CortinaReverse, x: 960, y: 550, scale: 1.2, timeout: 0, zOrder: 10, background: ''},
                ]
            })
        }, 2500);

    }

    componentWillUnmount() {
        this.props.app.stage.removeChildren();

        if(this.mountTimeOut){
            clearTimeout(this.mountTimeOut);
        }

        if(this.transitionTimeOut){
            clearTimeout(this.transitionTimeOut);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state === nextState) {
            return false;
        }

        return true
    }

    render() {

        return (

            <Container>
                {this.state.characters.map((t, index) => {
                    const TagName = t.component;
                    return <TagName x={t.x} key={"char" + index} y={t.y} scale={t.scale} color={t.color} fontSize={t.fontSize} timeout={t.timeout}
                                    background={t.background} app={this.app} resources={this._resources}/>
                })
                }

            </Container>

        )
    }
}

Games.propTypes = {
    options: PropTypes.object.isRequired,
    dictionary: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
}

const mapStateToProps = (state, ownProps) => {

    console.log(state.options);
    return {
        options: state.options,
        dictionary: state.dictionary,

    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...actions}, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Games)