import React from 'react';
import conejo_ske from '../../resource/conejo_bebe/conejobebe_ske';
import conejo_tex from '../../resource/conejo_bebe/conejobebe_tex.json';
import conejo_png from '../../resource/conejo_bebe/conejobebe_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class ConejoBebe extends React.Component {

    state = {rotation: 0}

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(conejo_ske);
        factory.parseTextureAtlasData(conejo_tex, PIXI.Texture.fromImage(conejo_png));

        const armatureDisplay = factory.buildArmatureDisplay("Armature", "conejobebe");
        armatureDisplay.animation.play("animtion0");
        armatureDisplay.x = this.props.x;
        armatureDisplay.y = this.props.y;
        armatureDisplay.scale.x = -this.props.scale;
        armatureDisplay.debugDraw = false;
        armatureDisplay.scale.y = this.props.scale;
        armatureDisplay.animation.timeScale = 1;
        setTimeout(() => {
            this.props.app.stage.addChild(armatureDisplay);
        }, this.props.timeout);
        console.log(this.props.app.stage);
        return null
    }
}

export default ConejoBebe;
