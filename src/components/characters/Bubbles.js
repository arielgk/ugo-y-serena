import React from 'react';
import bubble_ske from '../../resource/bubble/bubbles2_ske.json';
import bubble_tex from '../../resource/bubble/bubbles2_tex.json';
import bubble_png from '../../resource/bubble/bubbles2_tex.png';
import dragonBones from '../../dragonBones';
import * as PIXI from "pixi.js";

class Bubbles extends React.Component {

    constructor(props) {
        super(props);

        this.armatureDisplay = null;
    }

    componentDidMount() {
        this.props.app.ticker.add(this.tick)
        // this.props.app.stage.addChild(this.armatureDisplay);
    }

    componentWillUnmount() {
        this.props.app.ticker.remove(this.tick)
    }

    tick(delta) {

    }

    shouldComponentUpdate() {
        return true;
    }

    render() {
        const factory = dragonBones.PixiFactory.factory;
        factory.parseDragonBonesData(bubble_ske);
        factory.parseTextureAtlasData(bubble_tex, PIXI.Texture.fromImage(bubble_png));

        this.armatureDisplay = factory.buildArmatureDisplay("Armature", "bubbles2");
        this.armatureDisplay.x = this.props.x;
        this.armatureDisplay.y = this.props.y;

        this.armatureDisplay.scale.x = this.props.scale;
        this.props.app.stage.addChild(this.armatureDisplay);
        this.armatureDisplay.debugDraw = false;
        this.armatureDisplay.scale.y = this.props.scale;
        this.armatureDisplay.animation.timeScale = 4;
        const animationConfig = this.armatureDisplay.animation.animationConfig;
        animationConfig.name = "animtion0";
        animationConfig.animation = 'animtion0';
        animationConfig.playTimes = 1; // Loop play.

        animationConfig.position = 0.0;
        this.armatureDisplay.animation.playConfig(animationConfig);


        return null
    }
}

export default Bubbles;
